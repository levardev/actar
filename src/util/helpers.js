// 1 
export const handleAppData = async (params) => {
  if(!Object.getOwnPropertyNames(params).length > 0) return;
  const param_type = params.ecomm ? 'ecomm' : 'variant'; // param beginning
  const param_id = params.ecomm || params.variant; // param ID
  const payload = await fetchPayload(param_id, param_type); // API
  return payload;
};

// 2
export const fetchPayload = async (param_id, param_type) => {
  try {

    let { variant_id, variant_type } = extractVariantId(param_id);

    if(param_type === 'variant') {
      variant_id = await variantsApiDeprecate(param_id, variant_type)
    }

    const url = `https://6frow8qvw3.execute-api.us-east-1.amazonaws.com/dev/public/ecommerce/viewer/v6/?ecommerce_variant_id=${variant_id}`

    const response = await fetch(url);
    if(!response.ok) { throw new Error(`Fetch Call Cannot Be Made`)}
    const { ecommerce_search_response_hits_source } = await response.json();

    const current_variant = ecommerce_search_response_hits_source[variant_id]
    
    const store_settings = await fetchStoreSettings(variant_type, current_variant.store_id); // API
    
    return {current_variant, store_settings};
  } catch (error) {
    console.log('fetchPayload error', error);
  }
};

// 3
export const extractVariantId = (variant_id) => {

  if(variant_id.includes('bco_')) {
    return { variant_id: variant_id, variant_type: "bco" }
  } else if(variant_id.includes('cus_')) {
    return { variant_id: variant_id, variant_type: "cus" } 
  } else {
    return { variant_id: variant_id, variant_type: "shopify" }
  }
}

// 4
export const fetchStoreSettings = async (variant_type, store_id) => {
  try {
    const url = `https://c0l9hb8b28.execute-api.us-east-1.amazonaws.com/dev/public/store?store_id=${store_id}`;
    
    const response = await fetch(url);
    if(!response.ok) { throw new Error(`Fetch Call Cannot Be Made`)}
    const data = await response.json();
    return data;
  } catch (error) {
      console.log('fetchPayload error', error);
      return {
          result: {
              ar_experience: null,
              cLogo: null,
              dynamic_text_settings: { custom_text_status: false },
              ga_id: null,
              klaviyo_id: null,
              yotpo_id: null,
          }
      }
  }
};

const variantsApiDeprecate = async (variant_id_deprecate, variant_type) => {

  // custom variant ids are the same dont need to api call
  if (variant_type === 'cus') {
    return variant_id_deprecate;
  }
  
  const url = `https://6frow8qvw3.execute-api.us-east-1.amazonaws.com/dev/public/variant_id/swap?legacy_variant_id=${variant_type !== 'bco' ? variant_id_deprecate : variant_id_deprecate.slice(4)}`;

  const response = await fetch(url);

  if (!response.ok) throw new Error(`Variants Deprecated API cannot be called`);

  const { ecommerce_variant_id: _new_ecommerce_variant_id } = await response.json();

  return _new_ecommerce_variant_id
};