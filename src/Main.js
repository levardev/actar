import React from 'react';
import QOL from "./QOL.js";
// icons
import cart from './assets/cart-shopping-solid.svg';
import store from './assets/ReturnToShopping_BackArrow-01.svg';

const Main = ({ app_state }) => {
  const [qolStatus, setQolStatus] = React.useState(true);

  React.useEffect(() => {
    setInterval(() => {
      setQolStatus(false)
    }, 5000)
  }, []);

  const ar_type = app_state?.ar_experience?.ar_cta_type || ''

  return (
    <div className="Main">
      { qolStatus ? (
      <QOL />
      ) : (
      <div className="container">
          <div className="app_copy">
              <h1>{app_state.product_title}</h1>
              { app_state.variant_title !== "Default Title" ? 
                  ( <h6>{app_state.variant_title}</h6> ) : ( <h6>{ar_type === 'add_to_cart' ? 'Add to cart' : 'Back To Store' }</h6> )
              }
          </div>
          <div className="app_logo">
              <span className="icon_container">
                  <img src={ar_type === 'add_to_cart' ? cart : store} className="app-logo" alt="logo" />
              </span>
          </div>
      </div>
      )}
    </div>
  );
}

export default Main;
