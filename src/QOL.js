import React from 'react';
// Icons
import icon_1 from './assets/tipsIcon.png';
import icon_2 from './assets/adjust.png';
import icon_3 from './assets/rotate.png';

const QOL = () => {
    return (
        <div className="container">
            <div className="qol-container">
                <div className="qol-icon-container">
                    <img src={icon_1} className="qol-icon" alt="qol" />
                    <span className="qol-copy">Place</span>
                </div>
                <div className="qol-icon-container">
                    <img src={icon_2} className="qol-icon" alt="qol" />
                    <span className="qol-copy">Move</span>
                </div>
                <div className="qol-icon-container">
                    <img src={icon_3} className="qol-icon" alt="qol" />
                    <span className="qol-copy">Rotate</span>
                </div>
            </div>
        </div>
    );
}

export default QOL;
