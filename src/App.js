import React from 'react';
import { handleAppData } from "./util/helpers";
import { useLocation } from "react-router-dom";
import Main from "./Main.js";
// Helpers
import queryString from 'query-string';
import spinning_logo from './assets/loading.png';
// import logo from './assets/cart-shopping-solid.svg';

const initialState = { ready: false, title: "Wireless Headphones", subTitle: "Perfect for home stero" };

const App = () => {
    const [state, setState] = React.useState({
        ...initialState
    });
    const location = useLocation();
    
    const handleAppSettings = React.useCallback(async () => {
        if(state.ready) return
        const params = queryString.parse(location.search);
        const data = await handleAppData(params);
        
        setState((state) => ({ ready: !state.ready, ...data.current_variant, ...data.store_settings.result }))
    }, [location.search, state.ready]);
    
    React.useEffect(() => {
        handleAppSettings();
    }, [handleAppSettings]);

    return (
        <div className="App">
            { state.ready ? (
            <Main app_state={state}/>
            ) : (
            <div className='loading-container'>
                <img src={spinning_logo} alt="loading" />
            </div>
            )}
        </div>
    );
}

export default App;
